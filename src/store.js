import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"
import axios from 'axios';

Vue.use(Vuex)

// axios.defaults.headers.common = {
//     'Access-Control-Allow-Origin': '*',
//     'Content-Type': 'application/json',
// };

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        user_name: 'Default',
        auth: false,
        token: '',
        userData: {
            user: {
                name: ''
            }
        }
    },
    mutations: {
        update_user_name(state, name) {
            state.user_name = name
        },
        login (state, token){

            state.token = token
            state.auth = true

            axios.defaults.headers.common = {
                "enKrypt": token,
            };
        },
        logout (state){
            state.token = ''
            state.auth = false
            state.userData = {
                user: {
                    name: ''
                }
            }

            axios.defaults.headers.common = {
                "enKrypt": ''
            };
        },
        updateUser (state, userData) {
            state.userData = userData
        }
    },
    actions: {

    }
})