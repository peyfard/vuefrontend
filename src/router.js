import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/login.vue'
import About from './views/About.vue'
import store from './store'
import axios from 'axios';

Vue.use(Router)

function authCheck (to, from, next) {
    if (store.state.auth) {
        axios.get('http://vueapi.com/api/v1/me')
            .then(response => {
                // console.log(response.data.user);
                store.commit('updateUser', response.data);
                next()
            })
            .catch(e => {
                console.log(e.response.status);
                if(e.response.status === 401){
                    axios.post('http://vueapi.com/api/v1/auth/logout')
                        .then(response => {
                            console.log(response.data);
                            store.commit('logout');
                        })
                        .catch(e => {
                            console.log(e.response.data);
                        })
                }
                next({
                    path: '/login'
                })
            })
    }else{
        next({
            path: '/login'
        })
    }
}

export default new Router({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/about',
            name: 'about',
            component: About,
            beforeEnter: authCheck
        },
        {
            path: "*",
            component: {
                template: "404"
            }

        }
    ]
})
